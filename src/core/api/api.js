/* eslint-disable no-return-await */
/* eslint-disable arrow-body-style */
const API_BASE = "https://jogtracker.herokuapp.com/api/v1/";

export const sendPOST = async (path, data, token) => {
  return await fetch(API_BASE + path, {
    method: "POST",
    headers: {
      Accept: "application/json",
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(data),
  })
    .then((res) => res.json())
    .then((resData) => resData);
};

export const sendGET = async (path, token) => {
  return await fetch(API_BASE + path, {
    method: "GET",
    headers: {
      Accept: "application/json",
      Authorization: `Bearer ${token}`,
    },
  })
    .then((res) => res.json())
    .then((resData) => resData);
};
