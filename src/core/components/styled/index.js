import styled from "styled-components";

// eslint-disable-next-line import/prefer-default-export
export const PageWrapper = styled.div`
  max-width: 620px;
  margin: 0 auto;
  min-height: calc(100vh - 116px);
  display: flex;
  flex-direction: column;
  padding: 10px;
`;

export const Button = styled.div`
  border-radius: 36px;
  border: solid 3px white;
  color: white;
  font-size: 18px;
  font-weight: bold;
  text-align: center;
  cursor: pointer;
`;
