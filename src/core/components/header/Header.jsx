import React, { useContext, useEffect, useState } from "react";
import { useLocation } from "react-router";
import { Drawer, Hidden } from "@material-ui/core";
import {
  HeaderWrapper,
  HeaderLogo,
  HeaderDesktopContainer,
  HeaderDesktopLink,
  HeaderMobileLink,
  HeaderMoblieContainer,
  DrawerHeader,
  MenuButton,
  CloseButton,
  FilterButton,
  FilterBlock,
  MobileButtonContainer,
} from "./styles";

import Logo from "../../../assets/logo.svg";
import LogoGreen from "../../../assets/logo-green.png";
import { JogContext } from "../../services/jog.service";

function Header() {
  const location = useLocation();
  const [isMenuOpened, setIsMenuOpened] = useState(false);
  const [isFilterOpened, setIsFilterOpened] = useState(false);
  const [dateFrom, setDateFrom] = useState("");
  const [dateTo, setDateTo] = useState("");
  const jogContext = useContext(JogContext);

  useEffect(() => {
    const dateFromMs = new Date(dateFrom).getTime();
    const dateToMs = new Date(dateTo).getTime();
    jogContext.filterJogs(dateFromMs, dateToMs);
  }, [dateTo, dateFrom]);

  return (
    <>
      <HeaderWrapper>
        <HeaderLogo src={Logo} alt="Logobear" />
        <Hidden mdDown>
          <HeaderDesktopContainer
            className={location.pathname === "/login" ? "not-visible" : ""}
          >
            <HeaderDesktopLink
              href="/"
              className={location.pathname === "/" ? "selected" : ""}
            >
              JOGS
            </HeaderDesktopLink>
            <HeaderDesktopLink
              href="/info"
              className={location.pathname === "/info" ? "selected" : ""}
            >
              INFO
            </HeaderDesktopLink>
            <HeaderDesktopLink
              href="#"
              className={location.pathname === "/contact" ? "selected" : ""}
            >
              CONTACT US
            </HeaderDesktopLink>
            {location.pathname === "/" ? (
              <FilterButton
                onClick={() => setIsFilterOpened(!isFilterOpened)}
                className={isFilterOpened ? "active" : ""}
              />
            ) : (
              ""
            )}
          </HeaderDesktopContainer>
        </Hidden>
        <Hidden mdUp>
          <MobileButtonContainer style={{ display: "flex" }}>
            {location.pathname === "/" ? (
              <FilterButton
                onClick={() => setIsFilterOpened(!isFilterOpened)}
                className={isFilterOpened ? "active" : ""}
              />
            ) : (
              ""
            )}
            <MenuButton onClick={() => setIsMenuOpened(true)} />
          </MobileButtonContainer>
          <Drawer anchor="right" open={isMenuOpened} style={{ width: "100%" }}>
            <DrawerHeader>
              <HeaderLogo src={LogoGreen} alt="Logobear" />
              <CloseButton onClick={() => setIsMenuOpened(false)} />
            </DrawerHeader>
            <HeaderMoblieContainer>
              <HeaderMobileLink
                href="/"
                className={location.pathname === "/" ? "selected" : ""}
              >
                JOGS
              </HeaderMobileLink>
              <HeaderMobileLink
                href="/info"
                className={location.pathname === "/info" ? "selected" : ""}
              >
                INFO
              </HeaderMobileLink>
              <HeaderMobileLink
                href="#"
                className={location.pathname === "/contact" ? "selected" : ""}
              >
                CONTACT US
              </HeaderMobileLink>
            </HeaderMoblieContainer>
          </Drawer>
        </Hidden>
      </HeaderWrapper>
      {isFilterOpened ? (
        <FilterBlock>
          <label htmlFor="date">
            Date from
            <input
              value={dateFrom}
              name="date"
              type="date"
              onChange={(e) => {
                setDateFrom(e.target.value);
              }}
            />
          </label>
          <label htmlFor="date">
            Date to
            <input
              value={dateTo}
              name="date"
              type="date"
              onChange={(e) => {
                setDateTo(e.target.value);
              }}
            />
          </label>
        </FilterBlock>
      ) : (
        ""
      )}
    </>
  );
}

export default Header;
