import styled from "styled-components";
import { Button } from "../styled";
import MenuIcon from "../../../assets/menu.png";
import CloseIcon from "../../../assets/close-grey.svg";
import FilterIcon from "../../../assets/filter.svg";
import FilterActiveIcon from "../../../assets/filter-active.svg";

export const HeaderWrapper = styled.div`
  height: 116px;
  background-color: #7ed321;
  padding: 0 30px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const FilterBlock = styled.div`
  background-color: #eaeaea;
  height: 60px;
  display: flex;
  align-items: center;
  justify-content: center;

  * {
    margin: 10px;
  }
`;

export const HeaderLogo = styled.img`
  height: 50%;
`;

export const HeaderDesktopContainer = styled.div`
  min-width: 25%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  &.not-visible {
    display: none;
  }
`;

export const HeaderMoblieContainer = styled.div`
  width: 100vw;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  &.not-visible {
    display: none;
  }
`;

export const HeaderDesktopLink = styled.a`
  color: white;
  text-decoration: none;
  font-size: 14px;
  font-weight: bold;
  &.selected {
    text-decoration: underline;
  }
`;

export const HeaderMobileLink = styled.a`
  color: black;
  text-decoration: none;
  font-size: 28px;
  font-weight: bold;
  &.selected {
    color: #7ed321;
  }
`;

export const MenuButton = styled(Button)`
  background: url(${MenuIcon});
  height: 27px;
  width: 27px;
  background-size: cover;
  border: none;
  border-radius: 0;
`;

export const CloseButton = styled(Button)`
  background: url(${CloseIcon});
  height: 30px;
  width: 30px;
  background-size: contain;
  background-repeat: no-repeat;
  border: none;
  border-radius: 0;
`;

export const FilterButton = styled(Button)`
  background: url(${FilterIcon});
  height: 25px;
  width: 25px;
  background-size: contain;
  background-repeat: no-repeat;
  border: none;
  border-radius: 0;
  &.active {
    background: url(${FilterActiveIcon});
    background-size: contain;
    background-repeat: no-repeat;
  }
`;

export const DrawerHeader = styled.div`
  height: 80px;
  padding: 20px;
  display: flex;
  justify-content: space-between;
`;

export const MobileButtonContainer = styled.div`
  display: flex;
  justify-content: space-between;
  width: 80px;
`;
