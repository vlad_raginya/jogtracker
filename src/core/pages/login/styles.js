import styled from "styled-components";
import { Button } from "../../components/styled";

export const LoginContainer = styled.div`
  background-color: #e990f9;
  height: 380px;
  border-radius: 44px;
  margin: auto 0;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
`;

export const LoginBlockLogo = styled.img``;

export const LoginButton = styled(Button)`
  width: 110px;
  padding: 20px;
`;
