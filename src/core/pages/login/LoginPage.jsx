import { React, useContext } from "react";
import { useHistory } from "react-router";
import { PageWrapper } from "../../components/styled";
import { LoginBlockLogo, LoginContainer, LoginButton } from "./styles";
import BearFace from "../../../assets/bear-face.svg";
import { AuthContext } from "../../services/auth.service";

function LoginPage() {
  const authContext = useContext(AuthContext);
  const history = useHistory();
  const SignIn = async () => {
    if (await authContext.logIn()) {
      history.push("/");
    }
  };

  return (
    <PageWrapper>
      <LoginContainer>
        <LoginBlockLogo src={BearFace} alt="Login" />
        <LoginButton onClick={SignIn}>Let me in</LoginButton>
      </LoginContainer>
    </PageWrapper>
  );
}

export default LoginPage;
