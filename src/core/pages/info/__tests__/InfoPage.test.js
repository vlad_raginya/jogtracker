/* eslint-disable react/react-in-jsx-scope */
/* eslint-disable no-undef */
import { render, screen } from "@testing-library/react";
import InfoPage from "../InfoPage";

test("renders info page initially", () => {
  render(<InfoPage />);
  const textElement = screen.getByText(/INFO/i);
  expect(textElement).toBeInTheDocument();
});
