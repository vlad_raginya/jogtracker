import styled from "styled-components";
import { Button } from "../../components/styled";
import AddIcon from "../../../assets/add.svg";

export const PlusButton = styled(Button)`
  background: url(${AddIcon});
  height: 60px;
  width: 60px;
  position: fixed;
  right: 40px;
  bottom: 40px;
`;
export const AddButton = styled(Button)`
  color: #e990f9;
  border-color: #e990f9;
  padding: 20px 30px;
  margin-top: 100px;
`;

export const EmptyPageContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  margin: auto 0;
  height: 50%;
`;

export const NonEmptyPageContainer = styled.div`
  margin-bottom: 100px;
`;
