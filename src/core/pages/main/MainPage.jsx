import React, { useContext, useState } from "react";
import { PageWrapper } from "../../components/styled";
import JogItem from "./components/JogItem/JogItem";
import {
  AddButton,
  EmptyPageContainer,
  PlusButton,
  NonEmptyPageContainer,
} from "./styles";
import EmptyIcon from "../../../assets/empty-page.svg";
import AddJogForm from "./components/AddJogForm";
import { JogContext } from "../../services/jog.service";

function MainPage() {
  const [mode, setMode] = useState("");

  const jogContext = useContext(JogContext);

  const openAddingForm = () => {
    setMode("add");
  };

  const saveForm = async (data) => {
    await jogContext.addNewJog(data);
    setMode("");
  };

  const closeAddingForm = () => {
    setMode("");
  };

  if (mode === "add") {
    return (
      <PageWrapper>
        <AddJogForm closeForm={closeAddingForm} saveForm={saveForm} />
      </PageWrapper>
    );
  }

  return (
    <PageWrapper>
      {jogContext.filteredJogs.length > 0 ? (
        <NonEmptyPageContainer>
          {jogContext.filteredJogs.map((el, index) => (
            <JogItem
              key={`jog_${index + 1}`}
              distance={el.distance}
              time={el.time}
              speed={((el.distance / el.time) * 60).toFixed(2)}
              date={el.date}
            />
          ))}
          <PlusButton onClick={openAddingForm} />
        </NonEmptyPageContainer>
      ) : (
        <EmptyPageContainer>
          <img src={EmptyIcon} alt="Empty" />
          <h2 style={{ color: "#b0b0b0" }}>Nothing is here</h2>
          <AddButton onClick={openAddingForm}>Create your jog first</AddButton>
        </EmptyPageContainer>
      )}
    </PageWrapper>
  );
}

export default MainPage;
