/* eslint-disable react/react-in-jsx-scope */
/* eslint-disable no-undef */
import { render, screen, fireEvent } from "@testing-library/react";
import MainPage from "../MainPage";

test("renders empty main page initially", () => {
  render(<MainPage jogs={[]} setJogs={jest.fn()} />);
  const textElement = screen.getByText(/Nothing is here/i);
  expect(textElement).toBeInTheDocument();
});

test("doesn't renders empty main page if data passed in", () => {
  render(
    <MainPage
      jogs={[
        {
          speed: 1,
          distance: 1,
          time: 1,
          date: 11111111,
        },
      ]}
      setJogs={jest.fn()}
    />
  );
  const textElement = screen.queryByText(/Nothing is here/i);
  expect(textElement).not.toBeInTheDocument();
});

test("renders jog element if data passed in", () => {
  render(
    <MainPage
      jogs={[
        {
          speed: 1,
          distance: 1,
          time: 1,
          date: 11111111,
        },
      ]}
      setJogs={jest.fn()}
    />
  );
  const textElement = screen.queryByText(/Speed/i);
  expect(textElement).toBeInTheDocument();
});

test("renders form if state changed", () => {
  const page = render(<MainPage jogs={[]} setJogs={jest.fn()} />);
  const buttonElement = page.getByText("Create your jog first");
  fireEvent.click(buttonElement);
  const textElement = screen.queryByText(/Save/i);
  expect(textElement).toBeInTheDocument();
});

test("renders main page if form closed", () => {
  const page = render(<MainPage jogs={[]} setJogs={jest.fn()} />);
  const buttonElement = page.getByText("Create your jog first");
  fireEvent.click(buttonElement);
  const closeButtonElement = screen.queryByTestId("cancel_btn");
  fireEvent.click(closeButtonElement);
  const newButtonElement = page.getByText("Create your jog first");
  expect(newButtonElement).toBeInTheDocument();
});
