import React from "react";
import PropTypes from "prop-types";
import { format } from "date-fns";
import { JogItemContainer, JogItemIcon, JogItemInfoContainer } from "./styles";
import JogIcon from "../../../../../assets/icon.svg";

function JogItem(props) {
  const { speed, distance, time, date } = props;
  return (
    <JogItemContainer>
      <JogItemIcon src={JogIcon} />
      <JogItemInfoContainer>
        <span className="date">{format(new Date(date), "dd.MM.yyyy")}</span>
        <span>
          Speed: <span className="value">{speed}</span>
        </span>
        <span>
          Distance: <span className="value">{distance} km</span>
        </span>
        <span>
          Time: <span className="value">{time} min</span>
        </span>
      </JogItemInfoContainer>
    </JogItemContainer>
  );
}

JogItem.propTypes = {
  speed: PropTypes.string.isRequired,
  distance: PropTypes.number.isRequired,
  time: PropTypes.number.isRequired,
  date: PropTypes.number.isRequired,
};

export default JogItem;
