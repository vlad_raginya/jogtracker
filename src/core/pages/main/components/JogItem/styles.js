import styled from "styled-components";

export const JogItemContainer = styled.div`
  display: flex;
  padding: 50px;
  justify-content: center;
`;

export const JogItemIcon = styled.img`
  margin-right: 50px;
`;

export const JogItemInfoContainer = styled.div`
  display: flex;
  flex-direction: column;
  font-weight: 500;
  width: 200px;

  .date {
    font-weight: 400;
    color: #bababa;
  }

  .value {
    color: #b0b0b0;
  }
`;
