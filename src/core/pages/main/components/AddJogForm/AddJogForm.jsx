import React, { useState } from "react";
import PropTypes from "prop-types";
import { CancelButton, JogFormContainer, SaveButton } from "./styles";

function AddJogForm(props) {
  const { closeForm, saveForm } = props;
  const [distance, setDistance] = useState(0);
  const [time, setTime] = useState(0);
  const [date, setDate] = useState("");

  const handleSave = () => {
    saveForm({
      distance,
      time,
      date,
    });
  };

  return (
    <JogFormContainer>
      <CancelButton data-testid="cancel_btn" onClick={closeForm} />
      <label htmlFor="distance">
        Distance
        <input
          value={distance}
          name="distance"
          type="number"
          min="0"
          onChange={(e) => {
            setDistance(e.target.value);
          }}
        />
      </label>
      <label htmlFor="time">
        Time
        <input
          value={time}
          name="time"
          type="number"
          min="0"
          onChange={(e) => {
            setTime(e.target.value);
          }}
        />
      </label>
      <label htmlFor="date">
        Date
        <input
          value={date}
          name="date"
          type="date"
          onChange={(e) => {
            setDate(e.target.value);
          }}
        />
      </label>
      <SaveButton onClick={handleSave}>Save</SaveButton>
    </JogFormContainer>
  );
}

AddJogForm.propTypes = {
  closeForm: PropTypes.func.isRequired,
  saveForm: PropTypes.func.isRequired,
};

export default AddJogForm;
