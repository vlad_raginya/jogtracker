import styled from "styled-components";
import { Button } from "../../../../components/styled";
import CancelIcon from "../../../../../assets/cancel.svg";

export const JogFormContainer = styled.div`
  background-color: #7ed321;

  border-radius: 44px;
  margin: auto 0;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  padding: 30px;
  height: 320px;

  & > label {
    width: 70%;
    display: flex;
    justify-content: space-between;
    input {
      width: 90%;
      max-width: 300px;
    }
    @media screen and (max-width: 900px) {
      flex-direction: column;
      input {
        width: 100%;
        max-width: 400px;
      }
    }
  }
`;

export const CancelButton = styled(Button)`
  background: url(${CancelIcon});
  height: 27px;
  width: 27px;
  background-size: contain;
  background-repeat: no-repeat;
  border: none;
  border-radius: 0;
  align-self: flex-end;
`;

export const SaveButton = styled(Button)`
  width: 70%;
  padding: 15px;
`;
