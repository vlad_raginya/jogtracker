import React, { useContext, useEffect, useState } from "react";
import { sendGET, sendPOST } from "../api/api";
import { AuthContext } from "./auth.service";

export const JogContext = React.createContext(null);

// eslint-disable-next-line react/prop-types
const JogProvider = ({ children }) => {
  const [jogs, setJogs] = useState([]);
  const [filteredJogs, setFilteredJogs] = useState([]);
  const authContext = useContext(AuthContext);

  const filterJogs = (dateFrom, dateTo) => {
    setFilteredJogs(
      jogs
        .filter((el) => {
          if ((!!dateFrom && el.date > dateFrom) || !dateFrom) return true;
          return false;
        })
        .filter((el) => {
          if ((!!dateFrom && el.date < dateTo) || !dateTo) return true;
          return false;
        })
    );
  };

  const GetJogs = async () => {
    const token = authContext.getToken();
    const res = await sendGET("data/sync", token);
    setJogs(res.response.jogs);
    return res.response.jogs;
  };

  const AddNewJog = async (data) => {
    const token = authContext.getToken();
    const res = await sendPOST("data/jog", data, token);
    setJogs([...jogs, res.response]);
    return res.response;
  };

  useEffect(async () => {
    if (authContext.isLoggedIn) {
      const resJogs = await GetJogs();
      console.log(resJogs);
      setFilteredJogs(resJogs);
    }
  }, [authContext.isLoggedIn]);

  return (
    <JogContext.Provider
      value={{
        addNewJog: AddNewJog,
        getJogs: GetJogs,
        filterJogs: filterJogs,
        jogs: jogs,
        filteredJogs: filteredJogs,
      }}
    >
      {children}
    </JogContext.Provider>
  );
};

export default JogProvider;
