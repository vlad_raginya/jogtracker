import React, { useState, useEffect } from "react";
import { useHistory } from "react-router";
import { sendPOST } from "../api/api";

export const AuthContext = React.createContext(null);

// eslint-disable-next-line react/prop-types
const AuthProvider = ({ children }) => {
  const history = useHistory();
  const [IsLoggedInInternal, setIsLoggedInInternal] = useState(false);

  const GetToken = () => window.localStorage.getItem("auth_token");

  const IsLoggedIn = () => {
    const token = window.localStorage.getItem("auth_token");
    return !!token;
  };

  const LogIn = async () => {
    const res = await sendPOST("auth/uuidLogin", { uuid: "hello" });
    if (res.response.access_token) {
      window.localStorage.setItem("auth_token", res.response.access_token);
      setIsLoggedInInternal(true);
      return true;
    }
    return false;
  };

  useEffect(() => {
    if (IsLoggedIn()) {
      setIsLoggedInInternal(true);
    } else {
      setIsLoggedInInternal(false);
      history.push("/login");
    }
  }, [IsLoggedInInternal]);

  return (
    <AuthContext.Provider
      value={{
        logIn: LogIn,
        getToken: GetToken,
        isLoggedIn: IsLoggedInInternal,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export default AuthProvider;
