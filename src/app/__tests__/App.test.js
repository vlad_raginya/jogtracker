/* eslint-disable react/react-in-jsx-scope */
/* eslint-disable no-undef */
import { render, screen } from "@testing-library/react";
import App from "../App";

test("renders login page initially", () => {
  render(<App />);
  const linkElement = screen.getByText(/Let me in/i);
  expect(linkElement).toBeInTheDocument();
});
