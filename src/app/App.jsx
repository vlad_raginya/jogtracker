import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Header from "../core/components/header";
import MainPage from "../core/pages/main";
import LoginPage from "../core/pages/login";
import InfoPage from "../core/pages/info";
import AuthProvider from "../core/services/auth.service";
import JogProvider from "../core/services/jog.service";

function App() {
  return (
    <Router>
      <AuthProvider>
        <JogProvider>
          <Header />
          <Switch>
            <Route path="/login">
              <LoginPage />
            </Route>
            <Route path="/info">
              <InfoPage />
            </Route>
            <Route path="/">
              <MainPage />
            </Route>
          </Switch>
        </JogProvider>
      </AuthProvider>
    </Router>
  );
}

export default App;
